#include "file_operations/file_work.h"
#include "transform/rotation.h"
#include <stdio.h>
#include <stdlib.h>

void handle_error(enum operation_status status, struct image img);

int main(int argc, char* argv[]) {

    if (argc < 3) {
        printf("Not enough arguments\n");
        return 0;
    }

    const char* const in_filename = argv[1];
    const char* const out_filename = argv[2];

    struct image img = {0};

    enum operation_status status = read_bmp_file(in_filename, &img);

    if (status != SUCCESS) {
        handle_error(status, img);
    }

    status = rotate(&img);
    if (status != SUCCESS) {
        handle_error(status, img);
    } 

    status = write_bmp_file(out_filename, &img);
    if (status != SUCCESS) {
        handle_error(status, img);
    }

    free_image(img);
    printf("Success\n");

    return 0;
}
