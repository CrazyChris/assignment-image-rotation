#ifndef BMP_H
#define BMP_H

#include <inttypes.h>

#pragma pack(push, 1)
struct bmp_header 
{
        uint16_t signature;
        uint32_t file_size;
        uint32_t reserved;
        uint32_t offset;
        uint32_t header_size;
        uint32_t width;
        uint32_t height;
        uint16_t planes;
        uint16_t bit_count;
        uint32_t compression;
        uint32_t image_size;
        uint32_t ppmx;
        uint32_t ppmy;
        uint32_t clr_used;
        uint32_t clr_imoprtant;
};
#pragma pack(pop)

#endif
