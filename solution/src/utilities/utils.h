#ifndef UTILS_H
#define UTILS_H

#include <inttypes.h>

uint8_t get_padding(uint32_t width);

#endif
